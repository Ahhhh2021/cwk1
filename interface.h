#include <stdio.h>
#include "linkedlist.h"
#include "user_management.h"


void welcome();

void goodbye();

void initialOption();

//display all book in the given booklist
void display_all_books(const LinkedList *list);

void search_for_books();

void registeration();

void login();

void librarianOption();

//choose user option based on the login state of given user
void userOption(User *user);
