#ifndef LINKEDLIST_GUARD__H
#define LINKEDLIST_GUARD__H

#include <stdio.h>

//use struct node as nodes in linkedlist, its data can be any type
typedef struct _node {
	void *data; // use void type to store any type of data
	struct _node *next;
} Node;

typedef struct _linkedList {
	Node *head;
	Node *tail;
    int num; // the number of nodes in the linkedlist
} LinkedList;

void initializeList(LinkedList *list);

void addTail(LinkedList *list, void *data);

void deleteNode(LinkedList *list, Node *node);

#endif
