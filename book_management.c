#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "book_management.h"
#define BUF_LEN 16385

char usingToLower[128];

void initializeBook(Book *book)
{
    book->id = 0;
    book->copies = 0;
    book->loanNum = 0;
    memset(book->title, 0, sizeof(book->title));
    memset(book->author, 0, sizeof(book->author));
    book->year = 0;
}

//saves the database of books in the specified file
//returns 0 if books were stored correctly, or an error code otherwise
int store_books(const char* filename)
{
    FILE *fp;
    fp = fopen(filename, "w+");
	if (!fp) {
		fprintf(stderr, "Failed in opening %s\n", filename);
		return -1;
	}

    //store the number of books
    fprintf(fp, "%d\n\n", booklist.num);

    Node *current = booklist.head;
    while (current != NULL)
    {
        Book *book = current->data;
        //Book ID:
        fprintf(fp, "ID:%d\n", book->id);
        //Book Title:
        fprintf(fp, "Book Title:%s\n", book->title);
        //comma separated list of authors
        fprintf(fp, "Author:%s\n", book->author);
        // year of publication
        fprintf(fp, "Year:%d\n", book->year);
        //number of copies the library has
        fprintf(fp, "Copies:%d\n", book->copies);
        //number of copies the library has loaned to users
        fprintf(fp, "Copies on loan:%d\n\n", book->loanNum);

        current = current->next;
    }

    fclose(fp);

    return 0;
}

//loads the database of books from the specified file
//the file must have been generated by a previous call to store_books()
//returns 0 if books were loaded correctly, or an error code otherwise
int load_books(const char* filename)
{
    FILE *fp;
    fp = fopen(filename, "r");
    if (!fp){
        fprintf(stderr, "Failed in opening %s\n", filename);
        return -1;
    }

    initializeList(&booklist);

    char buf[BUF_LEN];
    if (fgets(buf, BUF_LEN, fp) != NULL){
        sscanf(buf, "%d\n", &booklist.num);
        //printf("the number of book is %d\n", booklist.num);
    }
    else {
        fprintf(stderr, "Error in reading: %s\n", filename);
        return -1;
    }

    int size = booklist.num;
    booklist.num = 0;
    for (int i = 0; i < size; i++)
    {
        fgets(buf, BUF_LEN, fp); //readin the extra line break
        Book *book = (Book*)malloc(sizeof(Book));

        fgets(buf, BUF_LEN, fp);
        sscanf(buf,"ID:%d\n", &book->id);

        char temp[BUF_LEN];
        fgets(buf, BUF_LEN, fp);
        sscanf(buf, "Book Title:\%[^\n]", temp);
        strcpy(book->title, temp);

        fgets(buf, BUF_LEN, fp);
        sscanf(buf, "Author:\%[^\n]", temp);
        strcpy(book->author, temp);

        fgets(buf, BUF_LEN, fp);
        sscanf(buf,"Year:%d\n", &book->year);

        fgets(buf, BUF_LEN, fp);
        sscanf(buf,"Copies:%d\n", &book->copies);

        fgets(buf, BUF_LEN, fp);
        sscanf(buf,"Copies on loan:%d\n", &book->loanNum);

        addTail(&booklist, book);
    }

    fclose(fp);
	return 0;
}

//adds a book to the ones available to the library
//returns 0 if the book could be added, or an error code otherwise
int add_book(Book *book)
{
    addTail(&booklist, book);
    return 0;
}

//removes a book from the library
//returns 0 if the book could be successfully removed, or an error code otherwise.
int remove_book(Node *node)
{
    Book *book = node->data;
    if (book->loanNum != 0)
    {
    	//the book is on loan, librarian can't remove it
        return 1;
    }
    book->id = 0;
    book->copies = 0;
    memset(book->title, 0, sizeof(book->title));
    memset(book->author, 0, sizeof(book->author));
    book->year = 0;
    free(book);
    deleteNode(&booklist, node);
    return 0;
}

//change the given string s to its lower case
char *string_tolower(const char *s)
{
    memset(usingToLower, 0, sizeof(usingToLower));
    int len = strlen(s);
    for (int i = 0; i < len; i++)
    {
        if (s[i] >= 'A' && s[i] <= 'Z')
        {
            usingToLower[i] = s[i] + 32;
        }
        else usingToLower[i] = s[i];
    }
    return usingToLower;
}

//finds books with a given title.
//returns a BookArray structure, where the field "array" is a newly allocated array of books, or null if no book with the
//provided title can be found. The length of the array is also recorded in the returned structure, with 0 in case
//array is the null pointer.
LinkedList* find_book_by_title (char *title)
{
    for (int i = 0; i < strlen(title); i++)
    {
        if (title[i] >= 'A' && title[i] <= 'Z')
        {
            title[i] = title[i] +  32;
        }
    }

    LinkedList *requiredBooks = (LinkedList *)malloc(sizeof(LinkedList));
    initializeList(requiredBooks);

    Node *node = booklist.head;
    while (node != NULL)
    {
        Book *cur = node->data;
        char *booktitle = string_tolower(cur->title);
        if (strstr(booktitle, title) != NULL)
        {
            addTail(requiredBooks, cur);
        }
        node = node->next;
    }
    if (requiredBooks->num == 0)
    {
        return NULL;
    }
    return requiredBooks;
}

//finds books with the given authors.
//returns a BookArray structure, where the field "array" is a newly allocated array of books, or null if no book with the
//provided title can be found. The length of the array is also recorded in the returned structure, with 0 in case
//array is the null pointer.
LinkedList* find_book_by_author (char *author)
{
    for (int i = 0; i < strlen(author); i++)
    {
        if (author[i] >= 'A' && author[i] <= 'Z')
        {
            author[i] = author[i] +  32;
        }
    }

    LinkedList *requiredBooks = (LinkedList *)malloc(sizeof(LinkedList));
    initializeList(requiredBooks);

    Node *node = booklist.head;

    while (node != NULL)
    {
        Book *cur = node->data;
        char *bookauthor = string_tolower(cur->author);
        if (strstr(bookauthor, author) != NULL)
        {
            addTail(requiredBooks, node->data);
        }
        node = node->next;
    }

    if (requiredBooks->num == 0)
    {
        return NULL;
    }
    return requiredBooks;
}

//finds books published in the given year.
//returns a BookArray structure, where the field "array" is a newly allocated array of books, or null if no book with the
//provided title can be found. The length of the array is also recorded in the returned structure, with 0 in case
//array is the null pointer.
LinkedList* find_book_by_year (unsigned int year)
{
    LinkedList *requiredBooks = (LinkedList *)malloc(sizeof(LinkedList));
    initializeList(requiredBooks);

    Node *node = booklist.head;
    while (node != NULL)
    {
        Book *cur = node->data;
        int bookyear = cur->year;
        if (year == bookyear)
        {
            addTail(requiredBooks, node->data);
        }
        node = node->next;
    }
    if (requiredBooks->num == 0)
    {
        return NULL;
    }
    return requiredBooks;
}

Book *find_book_by_id(int id)
{
    Node *node = booklist.head;
    while (node != NULL)
    {
        Book *cur = node->data;
        int bookid = cur->id;
        if (id == bookid)
        {
            return cur;
        }
        node = node->next;
    }
    return NULL;
}

// int main()
// {
	//printf("hh");
    //LinkedList booklist;
    // initializeList(&booklist);
    // //
    // Book *b1 = (Book *)malloc(sizeof(Book));
    // strcpy(b1->title, "book4");
    // strcpy(b1->author, "mike");
    // b1->id = 1;
    // addHead(&booklist, b1);
    //
    // Book *b = find_book_by_id(1);
    // printf("%s\n", b->title);
    //
    // Book *b = booklist.head->data;
    // printf("%s\n", b->title);
    //
	// Book *b2 = (Book *)malloc(sizeof(Book));
    // strcpy(b2->title, "book2");
    // addTail(&booklist, b2);
    //
	// Node *t = booklist.head->next;
	// b = t->data;
	// printf("%s\n", b->title);
    //
	// remove_book(t);
	// // printf("%d\n", booklist.num);
    //
    // Book *b3 = (Book *)malloc(sizeof(Book));
    // strcpy(b2->title, "book3");
    // strcpy(b2->author, "mike alen");
    // //addTail(&booklist, b2);
    // add_book(b3);
    //
    // t = booklist.tail;
	// b = t->data;
	// printf("!!%s\n", b->author);

    // LinkedList *requiredbook = find_book_by_author("mike");
    //
    // printf("%d\n", requiredbook->num);
    // t = requiredbook->head;
    // while (t != NULL)
    // {
    //     Book *want = t->data;
    //     printf("%s\n", want->title);
    //     t = t->next;
    // }

    //store_books("book.txt");
    // load_books("book.txt");
    // Node *t = booklist.tail;
	// Book *b = t->data;
	// printf("!!%s\n", b->author);

//     return 0;
// }
