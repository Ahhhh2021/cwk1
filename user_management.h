
#ifndef USER_MANAGEMENT_GUARD__H
#define USER_MANAGEMENT_GUARD__H

#include <stdio.h>
#include "book_management.h"

#define MAX_BORROW_NUM 50

LinkedList userlist;

typedef struct _User {
        char username[128]; //user name
        char password[128]; // password
		unsigned int borrowNum; // number of borrowed books
        int borrowedBooksID[MAX_BORROW_NUM]; // a array of borrowed books
}User;

// typedef struct _Admin {
//     unsigned int id;
//     char *password;
// } Admin;

void initializeUser(User *user);

//return 0 if the user login successfully, otherwise return 1;
int create_account(User *user);

//return 0 if the user login successfully, otherwise return 1;
int delete_account(Node *node);

User *find_user_by_username(char *username);

//saves the database of users in the specified file
//returns 0 if books were stored correctly, or an error code otherwise
int store_users(const char* filename);

int borrow_book(User *user, Book *book);

int return_book(User *user, int id);

//loads the database of users from the specified file
//the file must have been generated by a previous call to store_books()
//returns 0 if books were loaded correctly, or an error code otherwise
int load_users(const char *filename);

#endif
