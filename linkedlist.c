#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "linkedlist.h"

// learnt this method of achieving linkedlist in the book
// Reese, R. (2014). Deep Understanding of C Pointers. Posts and Telecom Press.

void initializeList(LinkedList *list)
{
	list->head = NULL;
	list->tail = NULL;
    list->num = 0;
}

void addTail(LinkedList *list, void *data)
{
	Node *node = (Node*)malloc(sizeof(Node));
	node->data = data;
	if (list->head == NULL)
	{
		list->head = node;
	}
	else
	{
		list->tail->next = node;
	}
	list->tail = node;
	node->next = NULL;
    list->num++;
}

void deleteNode(LinkedList *list, Node *node)
{
	if (node == list->head)
	{
		if (list->head->next == NULL)
		{
			list->head = NULL;
			list->tail = NULL;
		}
		else
		{
			list->head = list->head->next;
		}
	}
	else
	{
		Node *tmp = list->head;
		while (tmp != NULL && tmp->next != node)
		{
			tmp = tmp->next;
		}
		if (tmp != NULL)
		{
			tmp->next = node->next;
			if (tmp->next == NULL)
			{
				list->tail = tmp;
			}
		}
	}
	free(node); 
    list->num--;
}
