#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "user_management.h"

LinkedList userlist;
#define BUF_LEN 16385

void initializeUser(User *user)
{
    memset(user->username, 0, sizeof(user->username));
    memset(user->password, 0, sizeof(user->password));
    user->borrowNum = 0;
    memset(user->borrowedBooksID, 0, sizeof(user->borrowedBooksID));
}


int store_users(const char* filename)
{
    FILE *fp;
    fp = fopen(filename, "w+");
	if (!fp) {
		fprintf(stderr, "Failed in opening %s\n", filename);
		return -1;
	}

    //store the number of books
    fprintf(fp, "%d\n\n", userlist.num);

    Node *current = userlist.head;
    while (current != NULL)
    {
        User *user = current->data;
        //Username:
        fprintf(fp, "Username:%s\n", user->username);
        //Password
        fprintf(fp, "Password:%s\n", user->password);
        //The number of book he/she has borrowed:
        fprintf(fp, "Borrowed %d books\n", user->borrowNum);

        if (user->borrowNum > 0)
        {
            fprintf(fp, "The book ID list is as follows:\n");

            for (int i = 0; i < user->borrowNum; i++)
            {
                fprintf(fp, "%d\n", user->borrowedBooksID[i]);
            }
        }
		fprintf(fp, "\n");

        current = current->next;
    }

    fclose(fp);

    return 0;
}

int load_users(const char *filename)
{
    FILE *fp;
    fp = fopen(filename, "r");
    if (!fp){
        fprintf(stderr, "Failed in opening %s\n", filename);
        return -1;
    }

    initializeList(&userlist);

    char buf[BUF_LEN];
    if (fgets(buf, BUF_LEN, fp) != NULL){
        sscanf(buf, "%d\n", &userlist.num);
        //printf("the number of user is %d\n", userlist.num);
    }
    else {
        fprintf(stderr, "Error in reading: %s\n", filename);
        return -1;
    }

    int size = userlist.num;
    userlist.num = 0;
    for (int i = 0; i < size; i++)
    {
        fgets(buf, BUF_LEN, fp); //readin the extra line break
        User *user = (User*)malloc(sizeof(User));

        char temp[BUF_LEN];
        fgets(buf, BUF_LEN, fp);
        sscanf(buf, "Username:\%[^\n]", temp);
        strcpy(user->username, temp);

        fgets(buf, BUF_LEN, fp);
        sscanf(buf, "Password:\%[^\n]", temp);
        strcpy(user->password, temp);

        fgets(buf, BUF_LEN, fp);
        sscanf(buf,"Borrowed %d books\n", &user->borrowNum);

        if (user->borrowNum > 0)
        {
            fgets(buf, BUF_LEN, fp); // a line of hint sentence, you don't need to load it

            for (int j = 0; j < user->borrowNum; j++)
            {
                fgets(buf, BUF_LEN, fp);
                sscanf(buf,"%d\n", &user->borrowedBooksID[j]);
            }
        }

        addTail(&userlist, user);
    }

    fclose(fp);
	return 0;
}

User *find_user_by_username(char *username)
{
    Node *current = userlist.head;
    User *s = current->data;
    
    while (current != NULL)
    {
        User *user = current->data;
        if (strcmp(user->username, username) == 0)
        {
            return user;
        }
        current = current->next;
    }
    return NULL;
}

int create_account(User *user)
{
    addTail(&userlist, user);
    return 0;
}

int delete_account(Node *node)
{
    User *user = node->data;
    memset(user->username, 0, sizeof(user->username));
    memset(user->password, 0, sizeof(user->password));
    user->borrowNum = 0;
    memset(user->borrowedBooksID, 0, sizeof(user->borrowedBooksID));
    free(user);
    deleteNode(&userlist, node);
    return 0;
}

int borrow_book(User *user, Book *book)
{
    if ((book->copies - book->loanNum) <= 0)
    {
        return 1;
    }
    book->loanNum++;
    user->borrowedBooksID[user->borrowNum++] = book->id;
    return 0;
}

int return_book(User *user, int id)
{
    int hasFound = 0;
    for (int i = 0; i < user->borrowNum; i++)
    {
        if (user->borrowedBooksID[i] == id)
        {
            hasFound = 1;
            for (int j = i; j < user->borrowNum; j++)
            {
                user->borrowedBooksID[j] = user->borrowedBooksID[j + 1];
            }
            user->borrowNum--;
            break;
        }
    }

    if (hasFound == 0)
    {
        return 1;
    }

    Book *b = find_book_by_id(id);
    b->copies++;
    return 0;
}
