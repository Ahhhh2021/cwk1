#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "interface.h"

#include "user_management.h"


char ioption[128], uoption[128], loption[128], soption[128];

void welcome()
{
    printf("\nHello! Welcome to library!\n");
}

void goodbye()
{
    printf("\nThank you for using the library, goodbye!\n");
    printf("Have a nice day!\n\n");
}

void initialOption()
{
    printf("\nPlease choose an option: \n");
    printf("1) Register an account\n");
    printf("2) Login\n");
    printf("3) Search for books\n");
    printf("4) Display all books\n");
    printf("5) Quit\n");
    printf(" Option: ");

    memset(ioption, 0, sizeof(ioption));
    gets(ioption);
    if (strlen(ioption) == 0)
    {
        gets(ioption);
    }

    while (strlen(ioption) > 1 || ioption[0] > '0' + 5 || ioption[0] < '0')
    {
        printf("Sorry, the option you entered is invalid, please try again.\n");
        initialOption();
    }

    if (ioption[0] == '0' + 1)
    {
        registeration();

    }

    else if (ioption[0] == '0' + 2)
    {
        login();
    }

    else if (ioption[0] == '0' + 3)
    {
        search_for_books();
        initialOption();
    }

    else if (ioption[0] == '0' + 4)
    {
        display_all_books(&booklist);
        initialOption();
    }
    else if (ioption[0] == '0' + 5)
    {
    	store_users("user.txt");
    	store_books("book.txt");
        goodbye();
        exit(0);
    }
}

void display_all_books(const LinkedList *list)
{
    printf("\nID\t Title\t\t\t Authors\t\t Year\tCopies\t\n");

    Node *current = list->head;
    while (current != NULL)
    {
        Book *b = current->data;
        printf("%d\t ", b->id);
        if (strlen(b->title) < 7)
        {
            printf("%s\t\t\t ", b->title);
        }
        else if (strlen(b->title) < 15)
        {
            printf("%s\t\t ", b->title);
        }
        else if (strlen(b->title) < 23)
        {
            printf("%s\t ", b->title);
        }
        else
        {
            for (int i = 0; i < 20; i++)
            {
                printf("%c", b->title[i]);
            }
            printf("... ");
        }

        if (strlen(b->author) < 7)
        {
            printf("%s\t\t\t ", b->author);
        }
        else if (strlen(b->author) < 15)
        {
            printf("%s\t\t ", b->author);
        }
        else if (strlen(b->author) < 23)
        {
            printf("%s\t ", b->author);
        }
        else
        {
            for (int i = 0; i < 20; i++)
            {
                printf("%c", b->author[i]);
            }
            printf("... ");
        }
		
		//in this table, the copies column shows the copies that library has now
        printf("%d\t%d\t\n", b->year, b->copies - b->loanNum);
        current = current->next;
    }
}

void search_for_books()
{
    printf("\nPlease chooes an option: \n");
    printf("1) Find books by title\n");
    printf("2) Find books by author\n");
    printf("3) Find books by year\n");
    printf("4) Return to previous menu\n");
    printf(" Option: ");

    memset(soption, 0, sizeof(soption));
    gets(soption);
    if (strlen(soption) == 0)
    {
        gets(soption);
    }
    printf("\n");
    while (strlen(soption) > 1 || soption[0] > '0' + 4 || soption[0] < '0')
    {
        printf("Sorry, the option you entered is invalid, please try again.\n");
        search_for_books();
    }

    if (soption[0] == '0' + 1)
    {
        printf("\nPlease enter title: ");
        char wantedTitle[128];
        scanf("%s", wantedTitle);
        LinkedList *wantedBooklist = find_book_by_title(wantedTitle);
        if (wantedBooklist == NULL)
        {
            printf("Sorry, no book is found.\n");
            return;
        }
        else
        {
            display_all_books(wantedBooklist);
        }
    }
    else if (soption[0] == '0' + 2)
    {
        printf("\nPlease enter author: ");
        char wantedAuthor[128];
        scanf("%s", wantedAuthor);
        if (wantedAuthor[0] >= '0' && wantedAuthor[0] <= '9')
        {
            printf("The author name you entered is not correct.\n");
            search_for_books();
        }
        LinkedList *wantedBooklist = find_book_by_author(wantedAuthor);
        if (wantedBooklist == NULL)
        {
            printf("Sorry, no book is found.\n");
            return;
        }
        else
        {
            display_all_books(wantedBooklist);
        }
    }
    else if (soption[0] == '0' + 3)
    {
        printf("\nPlease enter year: ");
        char enterValue[128];
        gets(enterValue);
        if (enterValue[0] < '0' || enterValue[0] > '9')
        {
            printf("The publication year you entered is not correct.\n");
            search_for_books();
        }
        int wantedYear;
        sscanf(enterValue, "%d", &wantedYear);

        LinkedList *wantedBooklist = find_book_by_year(wantedYear);
  
        if (wantedBooklist == NULL)
        {
            printf("Sorry, no book is found.\n");
            return;
        }
        else
        {
            display_all_books(wantedBooklist);
        }
    }
    else if (soption[0] == '0' + 4)
    {
        return;
    }
}

void registeration()
{
    printf("\nPlease enter your username:");
    char username[128];
	gets(username);
	
    User *tempt = find_user_by_username(username);

    while (tempt != NULL)
    {
        printf("This username already exists. Please enter another one: ");
        scanf("%s", username);
        tempt = find_user_by_username(username);
    }
    tempt = (User *)malloc(sizeof(User));
    initializeUser(tempt);
    strcpy(tempt->username, username);

    printf("Please enter your password:");
    char password[128];
    gets(password);
    strcpy(tempt->password, password);

    create_account(tempt);
    printf("\nCongratulation! Registered library account successfully!\n");
    printf("\n(Logged in as: %s)\n", username);
    userOption(tempt);
}

void login()
{
    printf("\nPlease enter your username:");
    char username[128];
    gets(username);

    printf("Please enter your password:");
    char password[128];
    gets(password);

    if (strcmp(username, "librarian") == 0 && strcmp(password, "librarian") == 0)
    {
        printf("\n(Logged in as: %s)\n", username);
        librarianOption();
        return;
    }

    User *tempt = find_user_by_username(username);
    if (tempt == NULL || strcmp(tempt->password, password) != 0)
    {
        printf("Username or password incorrect. Please try again.\n");
        login();
    }

    printf("\nHi, %s!\n", username);
    
    userOption(tempt);
}

void librarianOption()
{
    printf("\nPlease choose an option: \n");
    printf("1) Add a book\n");
    printf("2) Remove a book\n");
    printf("3) Search for books\n");
    printf("4) Display all books\n");
    printf("5) Remove a user\n");
    printf("6) Log out\n");
    printf(" Option: ");

    memset(loption, 0, sizeof(loption));
    gets(loption);
    if (strlen(loption) == 0)
    {
        gets(loption);
    }
    printf("\n");
    
    while (strlen(loption) > 1 || loption[0] > '0' + 6 || loption[0] < '0')
    {
        printf("Sorry, the option you entered is invalid, please try again.\n");
        librarianOption();
    }

    if (loption[0] == '0' + 1)
    {
        Book *bookAdd = (Book *)malloc(sizeof(Book));
        bookAdd->id = booklist.num + 1;
        printf("\nEnter the title of the book you wish to add: ");
        char titleAdd[128];
        gets(titleAdd);
      
        strcpy(bookAdd->title, titleAdd);
        printf("Enter the author of the book you wish to add: ");
        char authorAdd[128];
        gets(authorAdd);
        strcpy(bookAdd->author, authorAdd);
       
        printf("Enter the year of the book you wish to add: ");
        scanf("%d", &bookAdd->year);
        printf("Enter the number of copies of the book you wish to add: ");
        scanf("%d", &bookAdd->copies);

        LinkedList *tmp = find_book_by_title(bookAdd->title);
       	
       	int hasSameBook = 0;
        if (tmp != NULL)
        {
            Node *node = tmp->head;
            while (node != NULL)
            {
                Book *cur = node->data;
                if (strcmp(cur->title, bookAdd->title) == 0 && strcmp(cur->author, bookAdd->author) == 0 && cur->year == bookAdd->year)
                {
                	hasSameBook = 1;
                    printf("This book has already been added. Would you want to add its copies? (y / n)\n");
                    
					char op[128];
                    gets(op);
                    if (strlen(op) == 0)
    				{
        				gets(op);
    				}
                    
                    if (op[0] == 'y')
                    {
                        cur->copies += bookAdd->copies;
                        printf("\nCopies of this book are successfully added!\n");
                    }
                    
                    break;
                }
                node = node->next;
            }
        }
        if (hasSameBook == 0)
        {
            add_book(bookAdd);
            printf("\nBook is successfully added!\n");
        }
        librarianOption();
    }
    else if (loption[0] == '0' + 2)
    {
        printf("\nEnter the ID of the book you wish to remove: ");
        int removeID;
        scanf("%d", &removeID);

        int found = 0;
        Node *node = booklist.head;
        while (node != NULL)
        {
            Book *cur = node->data;
            int bookid = cur->id;
            if (removeID == bookid)
            {
                if (remove_book(node) == 1)
                {
                    printf("\nThis book is on loan, you cannot remove it.\n");
                    found = 2;
                }
                else
                {
                    found = 1;
                }

                break;
            }
            node = node->next;
        }

        if (found == 0)
        {
            printf("\nSorry, the book was not been found.\n");
        }
        else if (found == 1)
        {
            printf("\nBook was successfully removed!\n");
        }
        
        librarianOption();
    }
    
    else if (loption[0] == '0' + 3)
    {
        search_for_books();
        librarianOption();
    }
    
    else if (loption[0] == '0' + 4)
    {
        display_all_books(&booklist);
        librarianOption();
    }
    
    else if (loption[0] == '0' + 5)
    {
        printf("\nEnter the username of the user you wish to remove: ");
        char userRemove[128];
        gets(userRemove);

        int found = 0;
        Node *node = userlist.head;
        while (node != NULL)
        {
            User *cur = node->data;
            if (strcmp(userRemove, cur->username) == 0)
            {
                delete_account(node);
                found = 1;
                break;
            }
            node = node->next;
        }

        if (found == 0)
        {
            printf("\nSorry, the user was not been found.\n");
            librarianOption();
        }
        else
        {
            printf("\nUser was successfully removed!\n");
            librarianOption();
        }
    }
    else if (loption[0] == '0' + 6)
    {
        initialOption();
    }
}

void userOption(User *user)
{
    printf("\nPlease choose an option: \n");
    printf("1) Borrow a book\n");
    printf("2) Return a book\n");
    printf("3) Search for books\n");
    printf("4) Display all books\n");
    printf("5) Display the books you have borrowed\n");
    printf("6) Log out\n");
    printf(" Option: ");

    memset(uoption, 0, sizeof(uoption));
    gets(uoption);
    if (strlen(uoption) == 0)
    {
        gets(uoption);
    }
    printf("\n");
    
    while (strlen(uoption) > 1 || uoption[0] > '0' + 6 || uoption[0] < '0')
    {
        printf("Sorry, the option you entered is invalid, please try again.\n");
        initialOption();
    }

    if (uoption[0] == '0' + 1)
    {
        if (user->borrowNum == MAX_BORROW_NUM)
        {
            printf("You have borrowed the maximum number of books!\n");
            userOption(user);
        }

        printf("\nEnter the ID number of the book you wish to borrow: ");
        int wantedID;
        scanf("%d", &wantedID);
        Book *wantedBook = find_book_by_id(wantedID);
        if (wantedBook == NULL)
        {
            printf("\nSorry, the book was not been found.\n");
        }

        if (borrow_book(user, wantedBook) == 0)
        {
            printf("\nYou have successfully borrowed the book!\n");
        }
        else
        {
            printf("\nSorry, this book is not available now.\n");
        }

        userOption(user);
    }
    else if (uoption[0] == '0' + 2)
    {
        printf("\nEnter the ID number of the book you wish to return: ");
        int wantedID;
        scanf("%d", &wantedID);
        if (return_book(user, wantedID) == 1)
        {
            printf("\nYou have not borrowed this book.\n");
            userOption(user);
        }
        else
        {
            printf("\nYou have successfully returned the book!\n");
        }

        userOption(user);
    }

    else if (uoption[0] == '0' + 3)
    {
        search_for_books();
        userOption(user);
    }

    else if (uoption[0] == '0' + 4)
    {
        display_all_books(&booklist);
        userOption(user);
    }
    else if (uoption[0] == '0' + 5)
    {
        if (user->borrowNum == 0)
        {
        	printf("\nYou have not borrowed books yet.\n");
		}
		else
		{
			LinkedList *borrowedBooklist = (LinkedList *)malloc(sizeof(LinkedList));
        	initializeList(borrowedBooklist);
			for (int i = 0; i < user->borrowNum; i++)
        	{
            	Book *book = find_book_by_id(user->borrowedBooksID[i]);
            	addTail(borrowedBooklist, book);
        	}
        	display_all_books(borrowedBooklist);
		}
       
        userOption(user);
    }
    else if (uoption[0] == '0' + 6)
    {
        initialOption();
    }
}

int main()
{
    load_users("user.txt");
    load_books("book.txt");

    welcome();
    initialOption();
    goodbye();

    store_users("user.txt");
    store_books("book.txt");
    return 0;
}
